extern crate gnm;

use gnm::messages::*;

use std::net::UdpSocket;
use std::io::{self, Read};
use std::thread;
use std::str;

struct GlitchClient {
    port: String,
    socket: UdpSocket
}

impl GlitchClient {
    pub fn init(port: &str) -> GlitchClient {
        let socket = match UdpSocket::bind("0.0.0.0:0") {
            Ok(s) => s,
            Err(e) => panic!("couldn't bind socket: {}", e)
        };
        GlitchClient { port: String::from(port), socket: socket }
    }

    pub fn send_message(&self, message: GameMessage) {
        let message_buffer = message.as_bytes();
        self.socket.send_to(&message_buffer, format!("0.0.0.0:{}", self.port)).unwrap();
    }

    pub fn receive_message(&self) {
        let mut buf = [0; 2048];
        match self.socket.recv_from(&mut buf) {
            Ok((amt, src)) => {
                thread::spawn(move || {
                    // println!("amt: {}", amt);
                    // println!("src: {}", src);
                    print!("{}", str::from_utf8(&buf).unwrap_or(""));
                });
            },
            Err(e) => {
                println!("couldn't recieve a datagram: {}", e);
            }
        }
    }
}

fn main() {
    let client = GlitchClient::init("5000");
    loop {
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(n) => {
                client.send_message(GameMessage { message: String::from("TEST") });
            }
            Err(error) => println!("error: {}", error),
        }
    }
}
