#[macro_use]
extern crate serde_derive;
extern crate bincode;

pub mod messages {
    use bincode::{serialize, deserialize};

    pub enum Message {
        Connected(String),
        Disconnected(String),
        Message(String)
    }

    #[derive(Serialize, Deserialize, Debug, PartialEq)]
    pub struct GameMessage {
        pub message: String
    }

    pub trait ToBytes {
        fn as_bytes(&self) -> Vec<u8>;
    }

    pub trait ToMessage {
        fn from_bytes(&self) -> GameMessage;
    }

    impl ToBytes for GameMessage {
        fn as_bytes(&self) -> Vec<u8> {
            serialize(&self).unwrap()
        }
    }

    impl ToMessage for [u8] {
        fn from_bytes(&self) -> GameMessage {
            deserialize(&self).unwrap()
        }
    }
}
