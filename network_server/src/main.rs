// use std::net::{SocketAddrV4, Ipv4Addr, TcpListener};
// use std::io::Read;
// use std::io::prelude::*;
//
// struct GlitchServer {
//     ip: [u8; 4],
//     port: u16
// }
//
// impl GlitchServer {
//     pub fn with_ip_and_port(ip_0: u8, ip_1: u8, ip_2: u8, ip_3: u8, port: u16) -> GlitchServer {
//         GlitchServer { ip: [ip_0, ip_1, ip_2, ip_3], port: port }
//     }
//
//     pub fn run(&self) -> Result<(), ()> {
//         let loopback = Ipv4Addr::new(self.ip[0], self.ip[1], self.ip[2], self.ip[3]);
//         let socket = SocketAddrV4::new(loopback, self.port);
//         let listener = TcpListener::bind(socket).unwrap();
//         let port = listener.local_addr().unwrap();
//
//         println!("Listening on {}, access this port to end the program", port);
//         let (mut tcp_stream, addr) = listener.accept().unwrap(); //block  until requested
//         println!("Connection received! {:?} is sending data.", addr);
//         let _ = tcp_stream.write(String::from("Daniel").as_bytes());
//
//         let mut input = String::new();
//         let _ = tcp_stream.read_to_string(&mut input).unwrap();
//         println!("{:?} says {}", addr, input);
//         Ok(())
//     }
// }
//
// // fn main() {
// //     let server = GlitchServer::with_ip_and_port(127, 0, 0, 1, 56677);
// //     let _result = server.run();
// // }

use std::str;
use std::thread;
use std::net::UdpSocket;

struct GlitchServer {
    port: String,
    socket: UdpSocket
}

impl GlitchServer {
    pub fn init(port: &str) -> GlitchServer {
        let socket = match UdpSocket::bind(format!("0.0.0.0:{}", port)) {
            Ok(s) => s,
            Err(e) => panic!("couldn't bind socket: {}", e)
        };
        GlitchServer { port: String::from(port), socket: socket }
    }

    pub fn listen(&self) {
        println!("Listening on port {}", self.port);
        loop {
            let mut buf = [0; 2048];
            match self.socket.recv_from(&mut buf) {
                Ok((amt, src)) => {
                    thread::spawn(move || {
                        // println!("amt: {}", amt);
                        // println!("src: {}", src);
                        print!("{}", str::from_utf8(&buf).unwrap_or(""));
                    });
                },
                Err(e) => {
                    println!("couldn't recieve a datagram: {}", e);
                }
            }
        }
    }
}

fn main() {
    let server = GlitchServer::init("5000");
    server.listen();
}
